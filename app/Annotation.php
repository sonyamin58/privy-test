<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Back End Engineer",
 *         @SWG\Contact(
 *             email="sonyamin58@gmail.com"
 *         ),
 *         @SWG\License(
 *             name="Private License"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about my website",
 *         url="http..."
 *     )
 * )
 */

/**
 * @SWG\Post(
 *   path="/v1/login",
 *      tags={"Auth"},
 *      operationId="Login",
 *      summary="Login",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="phone_number",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="password",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(property="token",type="string",description="token login"),
                @SWG\Property(
                    property="data", 
                    type="object",
                        @SWG\Property(property="name", type="string"),
                        @SWG\Property(property="phone_number", type="string"),
                        @SWG\Property(property="email", type="string"),
                )
            ),
        ),
 * )
*/

/**
 * @SWG\Get(
 *   path="/v1/user/",
 *      tags={"User"},
 *      operationId="User Profile",
 *      summary="User Profile",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(
                    property="data", 
                    type="object",
                        @SWG\Property(property="name", type="integer"),
                        @SWG\Property(property="email", type="string"),
                        @SWG\Property(property="user_phone", type="string"),
                        @SWG\Property(
                            property="balance", 
                            type="array",
                            @SWG\Items(
                                    @SWG\Property(property="balance", type="string"),
                                    @SWG\Property(property="balanceAchieve", type="string"),
                            )
                        )
                )
            ),
        ),
 * )
*/

/**
 * @SWG\Post(
 *   path="/v1/transaction/topup",
 *      tags={"Transaction"},
 *      operationId="Topup",
 *      summary="Topup",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="bank_code",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="amount",
 *          in="query",
 *          type="integer",
 *          required=true,
 *          @SWG\Schema(type="integer"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(property="msg",type="string",description="Top up berhasil melalui VA : 8807-phone_number"),
            ),
        ),
 * )
*/

/**
 * @SWG\Post(
 *   path="/v1/transaction/check-user",
 *      tags={"Transaction"},
 *      operationId="Check User",
 *      summary="Check User",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="phone_number",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="amount",
 *          in="query",
 *          type="integer",
 *          required=true,
 *          @SWG\Schema(type="integer"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(
                    property="data", 
                    type="object",
                        @SWG\Property(property="amount", type="integer"),
                        @SWG\Property(property="phone_number", type="string"),
                        @SWG\Property(property="name", type="string"),
                )
            ),
        ),
 * )
*/

/**
 * @SWG\Post(
 *   path="/v1/transaction/transfer-user",
 *      tags={"Transaction"},
 *      operationId="Transfer User",
 *      summary="Transfer User",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="phone_number",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="amount",
 *          in="query",
 *          type="integer",
 *          required=true,
 *          @SWG\Schema(type="integer"),
 *      ),
 *      @SWG\Parameter(
 *          name="password",
 *          in="query",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="integer"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(property="msg",type="string",description="Transfer berhasil ke : 081310496629"),
            ),
        ),
 * )
*/


/**
 * @SWG\Post(
 *   path="/v1/logout",
 *      tags={"Auth"},
 *      operationId="Logout",
 *      summary="Logout",
 *      security={{"x-api-key":{}}},
 *      @SWG\Parameter(
 *          name="x-api-key",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          type="string",
 *          required=true,
 *          @SWG\Schema(type="string"),
 *      ),
        @SWG\Response(
            response=200,
            description="Success",
            @SWG\Schema(
                @SWG\Property(property="result",type="boolean",description="status"),
                @SWG\Property(property="msg",type="string",description="status",example="Berhasil keluar")
            ),
        ),
 * )
*/

class AnnotationBastk extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
}