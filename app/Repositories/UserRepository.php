<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\BankBalance;
use App\Models\BankBalanceHistory; 
use App\Models\UserBalance;
use App\Models\UserBalanceHistory; 
use Illuminate\Database\QueryException;

class UserRepository
{
    public function user_by_phone($phone_number)
    {
        $user = User::where(["phone_number" => $phone_number])->first();
        return $user;
    }

    public function user_by_id($id){
    	$user = User::with('user_balance')->where(["id" => $id])->first();
    	return $user;
    }
}
