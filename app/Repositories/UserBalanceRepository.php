<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\BankBalance;
use App\Models\BankBalanceHistory; 
use App\Models\UserBalance;
use App\Models\UserBalanceHistory; 
use Illuminate\Database\QueryException;

class UserBalanceRepository
{
    public function sisa_limit_by_id($id)
    {	
        $user_balance = UserBalance::select('user_balance.balance', 'user_balance.balanceAchieve')
        	->join('users', 'users.id', '=', 'user_balance.user_id')
        	->where(["users.id" => $id])
        	->first();

        $sisa_limit = ($user_balance) ? $user_balance->balanceAchieve - $user_balance->balance : 0;
        return $sisa_limit;
    }

    public function sisa_limit_by_phone($phone_number)
    {	
        $user_balance = UserBalance::select('user_balance.balance', 'user_balance.balanceAchieve')
        	->join('users', 'users.id', '=', 'user_balance.user_id')
        	->where(["users.phone_number" => $phone_number])
        	->first();

        $sisa_limit = ($user_balance) ? $user_balance->balanceAchieve - $user_balance->balance : 0;
        return $sisa_limit;
    }

    public function sisa_saldo_by_id($id)
    {	
        $user_balance = UserBalance::select('user_balance.balance')
        	->join('users', 'users.id', '=', 'user_balance.user_id')
        	->where(["users.id" => $id])
        	->first();

        $sisa_saldo = ($user_balance) ? $user_balance->balance : 0;
        return $sisa_saldo;
    }

    public function transfer_user($phone_number, $amount, $user, $getIpAddress, $getLocation)
    {
        DB::beginTransaction();
        try {
            $userBalance = UserBalance::where('user_id', $user->id)->first();
            $userBalanceId = $userBalance->id;
            $userBalanceNow = $userBalance->balance;
            // update
            $userBalance->balance = $userBalanceNow - $amount;
            if ($userBalance->save()) {
                $userBalanceHistory = new UserBalanceHistory;
                $userBalanceHistory->userBalanceId = $userBalanceId;
                $userBalanceHistory->amount = $amount;
                $userBalanceHistory->balanceBefore = $userBalanceNow;
                $userBalanceHistory->balanceAfter = $userBalanceNow - $amount;
                $userBalanceHistory->ip = $getIpAddress;
                $userBalanceHistory->location = $getLocation;
                $userBalanceHistory->activity = 'Transfer';
                $userBalanceHistory->type = 'kredit';
                $userBalanceHistory->userAgent = $phone_number;
                $userBalanceHistory->author = $user->name;
                $userBalanceHistory->save();
            }

            $userBalanceAgent = UserBalance::select('user_balance.id', 'user_balance.balance')
                ->join('users', 'users.id', '=', 'user_balance.user_id')
                ->where('phone_number', $phone_number)
                ->first();
            $userBalanceAgentId = $userBalanceAgent->id;
            $userBalanceAgentNow = $userBalanceAgent->balance;
            // update
            $userBalanceAgent->balance = $userBalanceAgentNow + $amount;
            if ($userBalanceAgent->save()) {
                $userBalanceAgentHistory = new UserBalanceHistory;
                $userBalanceAgentHistory->userBalanceId = $userBalanceAgentId;
                $userBalanceAgentHistory->amount = $amount;
                $userBalanceAgentHistory->balanceBefore = $userBalanceAgentNow;
                $userBalanceAgentHistory->balanceAfter = $userBalanceAgentNow + $amount;
                $userBalanceAgentHistory->ip = $getIpAddress;
                $userBalanceAgentHistory->location = $getLocation;
                $userBalanceAgentHistory->activity = 'Transfer';
                $userBalanceAgentHistory->type = 'debit';
                $userBalanceAgentHistory->userAgent = $phone_number;
                $userBalanceAgentHistory->author = $user->name;
                $userBalanceAgentHistory->save();
            }

            DB::commit();
            return true;
        } catch (QueryException $e) {
            DB::rollback();
            return $e;
        }
    }
}
