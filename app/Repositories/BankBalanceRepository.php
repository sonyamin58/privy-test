<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\BankBalance;
use App\Models\BankBalanceHistory; 
use App\Models\UserBalance;
use App\Models\UserBalanceHistory; 
use Illuminate\Database\QueryException;

class BankBalanceRepository
{
    public function bank_code_list()
    {	
    	$bank = array();
        $bank_balance = BankBalance::select('code', 'balance')->groupBy('code')->get();
        foreach ($bank_balance as $value) {
        	$bank['code'][] = $value->code;
        	$bank['balance'][$value->code] = $value->balance;
        }
        return $bank;
    }

    public function top_up_user($bank_code, $amount, $user, $getIpAddress, $getLocation)
    {
    	DB::beginTransaction();
    	try {
    		$bankBalance = BankBalance::where('code', $bank_code)->first();
    		$bankBalanceId = $bankBalance->id;
    		$bankBalanceNow = $bankBalance->balance;
    		// update
    		$bankBalance->balance = $bankBalanceNow - $amount;
    		if ($bankBalance->save()) {
	    		$bankBalanceHistory = new BankBalanceHistory;
	    		$bankBalanceHistory->bankBalanceId = $bankBalanceId;
	    		$bankBalanceHistory->amount = $amount;
	    		$bankBalanceHistory->balanceBefore = $bankBalanceNow;
	    		$bankBalanceHistory->balanceAfter = $bankBalanceNow - $amount;
	    		$bankBalanceHistory->ip = $getIpAddress;
	    		$bankBalanceHistory->location = $getLocation;
	    		$bankBalanceHistory->activity = 'Top Up';
	    		$bankBalanceHistory->type = 'kredit';
	    		$bankBalanceHistory->userAgent = env('VA_CODE').$user->phone_number;
	    		$bankBalanceHistory->author = 'system';
	    		$bankBalanceHistory->save();
	    	}

	    	$userBalance = UserBalance::where('user_id', $user->id)->first();
    		$userBalanceId = $userBalance->id;
    		$userBalanceNow = $userBalance->balance;
    		// update
    		$userBalance->balance = $userBalanceNow + $amount;
    		if ($userBalance->save()) {
	    		$userBalanceHistory = new UserBalanceHistory;
	    		$userBalanceHistory->userBalanceId = $userBalanceId;
	    		$userBalanceHistory->amount = $amount;
	    		$userBalanceHistory->balanceBefore = $userBalanceNow;
	    		$userBalanceHistory->balanceAfter = $userBalanceNow + $amount;
	    		$userBalanceHistory->ip = $getIpAddress;
	    		$userBalanceHistory->location = $getLocation;
	    		$userBalanceHistory->activity = 'Top Up';
	    		$userBalanceHistory->type = 'debit';
	    		$userBalanceHistory->userAgent = $bank_code;
	    		$userBalanceHistory->author = 'system';
	    		$userBalanceHistory->save();
	    	}

    		DB::commit();
            return true;
    	} catch (QueryException $e) {
    		DB::rollback();
            return $e;
    	}
    }
}
