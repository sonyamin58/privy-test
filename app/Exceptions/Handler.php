<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
		\League\OAuth2\Server\Exception\OAuthServerException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
		/*$res["result"] = false;
		if($e instanceof AuthorizationException ){
			$res["msg"] = "Unauthorized";
			return response()->json($res, $e->getStatusCode());
		}
		
		//$res["msg"] = $e->getMessage()==""?"Bad Request":$e->getMessage();
		//return response()->json($res, $e->getStatusCode());
        return parent::render($request, $e);*/
		$rendered = parent::render($request, $e);

		$res["result"] = false;
		$res["msg"] = $e->getMessage();
		return response()->json($res, $rendered->getStatusCode());
		
		/*return response()->json([
			'error' => [
				'code' => $rendered->getStatusCode(),
				'message' => $e->getMessage(),
			]
		], $rendered->getStatusCode());*/
    }
}
