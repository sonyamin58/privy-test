<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalanceHistory extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table = "user_balance_history";
    protected $primaryKey = "id";
}
