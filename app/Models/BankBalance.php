<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankBalance extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table = "bank_balance";
    protected $primaryKey = "id";
}
