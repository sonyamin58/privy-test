<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankBalanceHistory extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table = "bank_balance_history";
    protected $primaryKey = "id";
}
