<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    /**
     * The table name
     *
     * @var string
     */
    protected $table = "user_balance";
    protected $primaryKey = "id";
}
