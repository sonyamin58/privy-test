<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
	public $ipAddress;
	public $location;

	public function __construct() {
		$this->ipAddress = null;
		$this->location = null;	
	}	

	public function setIpAddress($request)
	{
	    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
	        if (array_key_exists($key, $request) === true){
	            foreach (explode(',', $request[$key]) as $ip){
	                $ip = trim($ip); // just to be safe
	                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
	                    $this->ipAddress = $ip;
	                }
	            }
	        }
	    }
	    $this->ipAddress = request()->ip(); // it will return server ip when no client ip found
	}

	public function getIpAddress()
	{
		return $this->ipAddress;
	}

	public function setLocation($ipAddress)
	{
		$result = json_decode(file_get_contents("http://ipinfo.io/{$ipAddress}/json"));
		$this->location = serialize($result);
	}

	public function getLocation()
	{
		return $this->location;
	}
}