<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use App\Repositories\UserBalanceRepository;
use App\Repositories\BankBalanceRepository;

class TopUpController extends Controller 
{
    private $auth;
    private $userBalanceRepository;
    private $bankBalanceRepository;

    public function __construct(UserBalanceRepository $userBalanceRepository, BankBalanceRepository $bankBalanceRepository)
    {
        $this->auth = Auth::user();
        $this->userBalanceRepository = $userBalanceRepository;
        $this->bankBalanceRepository = $bankBalanceRepository;   
    }

    public function topup(Request $request)
    {
        $balanceAchieve = $this->userBalanceRepository->sisa_limit_by_id($this->auth->id);
        $bankCodeList   = $this->bankBalanceRepository->bank_code_list();
        
        $validator = Validator::make($request->all(), [ 
            'amount' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($balanceAchieve){
                    if ($value < 10000) {
                        $fail('Top up minimal Rp. 10.000.');
                    }
                    if ($value > $balanceAchieve) {
                        $fail('Top up maximal Rp. '.number_format($balanceAchieve).'.');
                    }
                }
            ],
            'bank_code' => [
                'required',
                'numeric', 
                function ($attribute, $value, $fail) use ($bankCodeList, $request){
                    if (!in_array($value, $bankCodeList['code'])) {
                        $fail('Code BANK tidak ditemukan.');
                    } else {
                        if ($request['amount'] > $bankCodeList['balance'][$value]) {
                            $fail('Maaf BANK sedang sibuk.');   
                        }
                    }
                }
            ],
        ]);
        if ($validator->fails()) { 
            $res["result"] = false;
            $res["msg"] = $validator->messages()->all();
            return response()->json($res, 400);
        }
        
        $amount = $request['amount'];
        $bank_code = $request['bank_code'];

        $setIpAddress = $this->setIpAddress($_SERVER);
        $getIpAddress = $this->getIpAddress();
        $setLocation = $this->setLocation($getIpAddress);
        $getLocation = $this->getLocation();
        
        $topup = $this->bankBalanceRepository->top_up_user($bank_code, $amount, $this->auth, $getIpAddress, $getLocation);
        if ($topup === true) {
            $res["result"] = true;
            $res["msg"] = 'Top up berhasil melalui VA : '.env('VA_CODE').$this->auth->phone_number;
            return response()->json($res, 400);
        }

        $res["result"] = false;
        $res["msg"] = $topup;
        return response()->json($res, 400); 
    }
}

