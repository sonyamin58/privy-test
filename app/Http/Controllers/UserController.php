<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\UserRepository;

class UserController extends Controller 
{
    private $auth;
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->auth = Auth::user();
        $this->userRepository = $userRepository;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'phone_number'  => 'required|digits_between:10,13|numeric', 
            'password'      => 'required|digits_between:6,6|numeric', 
        ]);
        if ($validator->fails()) { 
            $res["result"] = false;
            $res["msg"] = $validator->messages()->all();
            return response()->json($res, 400);
        }
        
        $phone_number = $request['phone_number'];
        $password = $request['password'];
        $user = $this->userRepository->user_by_phone($phone_number);
        if ($user) {
            $user->tokens()->where('id', '=', $user->id)->delete();
            if ($user->active == 0) {
                $res["result"] = false;
                $res["msg"] = "Tidak resmi. Akun Anda telah dinonaktifkan";
                return response()->json($res, 401); 
            } else {
                if ( Hash::check($password, $user->password) ) {
                    $res["result"]  = true;
                    $res['token']   = $user->createToken(env('APP_NAME', 'API'))->accessToken;
                    $res["data"]    = $user;
                    return response()->json($res, 200);                 
                } else {
                    $res["result"] = false;
                    $res["msg"] = "Username atau password salah.";
                    return response()->json($res, 401); 
                }
            }
        } else {
            $res["result"] = false;
            $res["msg"] = "Username atau password salah.";
            return response()->json($res, 401); 
        }
    }
    
    public function profile(Request $request)
    {
        $user = $this->userRepository->user_by_id($this->auth->id);
        return response()->json($user, 200); 
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        $res["result"] = true;
        $res["msg"] = "Berhasil logout";
        return response()->json($res, 200);
    }   
}

