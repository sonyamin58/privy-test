<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Validator; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Hash;
use App\Repositories\UserBalanceRepository;
use App\Repositories\UserRepository;

class TransferController extends Controller 
{
    private $auth;
    private $userBalanceRepository;
    private $userRepository;

    public function __construct(UserBalanceRepository $userBalanceRepository, UserRepository $userRepository)
    {
        $this->auth = Auth::user();
        $this->userBalanceRepository = $userBalanceRepository;
        $this->userRepository = $userRepository;   
    }

    public function check_user(Request $request)
    {
        $balanceNow = $this->userBalanceRepository->sisa_saldo_by_id($this->auth->id);
        $validator = Validator::make($request->all(), [ 
            'amount' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($balanceNow){
                    if ($value > $balanceNow) {
                        $fail('Saldo anda tidak cukup.');
                    }
                }
            ],
            'phone_number' => [
                'required',
                'numeric', 
                function ($attribute, $value, $fail) use ($request){
                    $balanceAgent = $this->userRepository->user_by_phone($value);
                    if (!$balanceAgent) {
                        $fail('Nomor penerima tidak ditemukan.');
                    } else {
                        $balanceAchieve = $this->userBalanceRepository->sisa_limit_by_phone($value);
                        if ($balanceAchieve < @$request['amount']) {
                            $fail('Saldo penerima akan melampaui batas.');
                        }
                    }
                }
            ],
        ]);
        if ($validator->fails()) { 
            $res["result"] = false;
            $res["msg"] = $validator->messages()->all();
            return response()->json($res, 400);
        }

        $amount = $request['amount'];
        $phone_number = $request['phone_number'];

        $user_tujuan = $this->userRepository->user_by_phone($phone_number);
        $res["result"] = true;
        $res["data"] = [
            'phone_number' => $phone_number,
            'amount' => $amount,
            'name' => $user_tujuan->name,
        ];

        return response()->json($res, 200);   
    }

    public function transfer_user(Request $request)
    {
        $balanceNow = $this->userBalanceRepository->sisa_saldo_by_id($this->auth->id);
        $validator = Validator::make($request->all(), [ 
            'password' => 'required|numeric|digits_between:6,6',
            'amount' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($balanceNow){
                    if ($value > $balanceNow) {
                        $fail('Saldo anda tidak cukup.');
                    }
                }
            ],
            'phone_number' => [
                'required',
                'numeric', 
                function ($attribute, $value, $fail) use ($request){
                    $balanceAgent = $this->userRepository->user_by_phone($value);
                    if (!$balanceAgent) {
                        $fail('Nomor penerima tidak ditemukan.');
                    } else {
                        $balanceAchieve = $this->userBalanceRepository->sisa_limit_by_phone($value);
                        if ($balanceAchieve < @$request['amount']) {
                            $fail('Saldo penerima akan melampaui batas.');
                        }
                    }
                }
            ],
        ]);
        if ($validator->fails()) { 
            $res["result"] = false;
            $res["msg"] = $validator->messages()->all();
            return response()->json($res, 400);
        }

        $amount = $request['amount'];
        $phone_number = $request['phone_number'];
        $password = $request['password'];
        if (!Hash::check($password, $this->auth->password)) {
            $res["result"] = false;
            $res['msg'] = 'Kesalahan password';
            return response()->json($res, 400);                 
        }

        $setIpAddress = $this->setIpAddress($_SERVER);
        $getIpAddress = $this->getIpAddress();
        $setLocation = $this->setLocation($getIpAddress);
        $getLocation = $this->getLocation();
        
        $transfer = $this->userBalanceRepository->transfer_user($phone_number, $amount, $this->auth, $getIpAddress, $getLocation);
        if ($transfer === true) {
            $res["result"] = true;
            $res["msg"] = 'Transfer berhasil ke : '.$phone_number;
            return response()->json($res, 200);
        }

        $res["result"] = false;
        $res["msg"] = $transfer;
        return response()->json($res, 400); 
    }
}

