<?php

namespace App\Http\Middleware;

use Closure;

class UserActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if ($request->user()->active==1) {
			return $next($request);
		} else {
			return abort(401, "Tidak resmi. Akun Anda telah dinonaktifkan");
		}
    }
}
