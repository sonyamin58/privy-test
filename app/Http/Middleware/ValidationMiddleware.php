<?php

namespace App\Http\Middleware;

use Closure;

class ValidationMiddleware
{
	
	public function isJSON($string) {
	   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	public function cleanObjPost($obj, $type) {
		foreach($obj as $key=>$val){
			if( is_array($val) || is_object($val) ){
				$type_detail = (is_array($val)?'arr':'obj');
				if( $type=='arr' ){
					$obj[$key] = $this->cleanObjPost($val, $type_detail);
				}else{
					$obj->$key = $this->cleanObjPost($val, $type_detail);
				}
			}else{
				if( $type=='arr' ){
					$obj[$key] = htmlentities($val);
				}else{
					$obj->$key = htmlentities($val);
				}
			}
		}

		return $obj;
	}	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$valid = false;
		if( $request->headers->has('x-api-key') ){
			if( $request->header('x-api-key')===env('API_KEY', 'KOSONG') ){
				foreach($_POST as $key=>$val){
					if( $this->isJSON($val) ){
						$obj = json_decode($val);
						$type = (is_array($obj)?'arr':'obj');
						$obj = $this->cleanObjPost($obj, $type);
						$_POST[$key] = json_encode($obj);
					}else{
						$_POST[$key] = htmlentities($val);
					}
				}
				$valid = true;
			}
		}
				
		if( $valid ){
			return $next($request);
		}else{
			return abort(412, "Invalid token. Aplikasi baru tersedia, silakan download.");
		}
    }
}
