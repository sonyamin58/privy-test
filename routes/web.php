<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


Route::group(['middleware' => ['x-api-key']], function(){
	Route::group(['prefix' => 'v1'], function(){
		Route::post('login', 'UserController@login');

		Route::group(['middleware' => ['auth:api', 'user.active']], function(){
			Route::post('logout', 'UserController@logout');
			
			Route::group(['prefix' => 'user'], function(){
				Route::get('/', 'UserController@profile');
			});

			Route::group(['prefix' => 'transaction'], function(){
				Route::post('/topup', 'TopUpController@topup');
				Route::post('/check-user', 'TransferController@check_user');
				Route::post('/transfer-user', 'TransferController@transfer_user');
			});
		});
	});
});