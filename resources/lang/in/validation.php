<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    |  following language lines contain  default error messages used by
    |  validator class. Some of se rules have multiple versions such
    | as  size rules. Feel free to tweak each of se messages here.
    |
    */

    'accepted'             => ' :attribute harus diterima.',
    'active_url'           => ' :attribute bukan URL yang valid.',
    'after'                => ' :attribute harus menjadi tanggal sesudah :date.',
    'after_or_equal'       => ' :attribute harus berupa tanggal setelah atau sama dengan :date.',
    'alpha'                => ' :attribute hanya boleh mengandung huruf.',
    'alpha_dash'           => ' :attribute hanya boleh berisi huruf, angka, dan garis putus-putus.',
    'alpha_num'            => ' :attribute hanya boleh berisi huruf dan angka.',
    'array'                => ' :attribute harus berupa array.',
    'before'               => ' :attribute harus tanggal sebelum :date.',
    'before_or_equal'      => ' :attribute harus berupa tanggal sebelum atau sama dengan :date.',
    'between'              => [
        'numeric' => ' :attribute harus antara :min dan :max.',
        'file'    => ' :attribute harus antara :min dan :max kilobytes.',
        'string'  => ' :attribute harus antara :min dan :max characters.',
        'array'   => ' :attribute harus ada di antara :min dan :max item.',
    ],
    'boolean'              => ' :attribute bidang harus benar atau salah.',
    'confirmed'            => ' :attribute konfirmasi tidak cocok.',
    'date'                 => ' :attribute bukan tanggal yang valid.',
    'date_format'          => ' :attribute tidak cocok dengan format :format.',
    'different'            => ' :attribute dan :or pasti berbeda.',
    'digits'               => ' :attribute harus :digits digit.',
    'digits_between'       => ' :attribute harus antara :min dan :max digit.',
    'dimensions'           => ' :attribute memiliki dimensi gambar yang tidak valid.',
    'distinct'             => ' :attribute bidang memiliki nilai duplikat.',
    'email'                => ' :attribute Harus alamat e-mail yang valid.',
    'exists'               => ' :attribute yang dipilih tidak valid.',
    'file'                 => ' :attribute harus berupa file.',
    'filled'               => ' :attribute  wajib diisi.',
    'image'                => ' :attribute harus berupa gambar.',
    'in'                   => ' :attribute yang dipilih tidak valid.',
    'in_array'             => ' :attribute  tidak ada di :or.',
    'integer'              => ' :attribute harus integer.',
    'ip'                   => ' :attribute harus alamat IP yang valid.',
    'json'                 => ' :attribute harus berupa string JSON yang valid.',
    'max'                  => [
        'numeric' => ' :attribute mungkin tidak lebih besar dari :max.',
        'file'    => ' :attribute mungkin tidak lebih besar dari :max kilobytes.',
        'string'  => ' :attribute mungkin tidak lebih besar dari :max karakter.',
        'array'   => ' :attribute mungkin tidak memiliki lebih dari :max items.',
    ],
    'mimes'                => ' :attribute harus berupa file type: :values.',
    'mimetypes'            => ' :attribute harus berupa file type: :values.',
    'min'                  => [
        'numeric' => ' :attribute minimal harus :min.',
        'file'    => ' :attribute minimal harus :min kilobytes.',
        'string'  => ' :attribute minimal harus :min karakter.',
        'array'   => ' :attribute minimal harus :min items.',
    ],
    'not_in'               => ' :attribute yang dipilih tidak valid.',
    'numeric'              => ' :attribute harus berupa angka.',
    'present'              => ' :attribute field harus ada.',
    'regex'                => ' :attribute format tidak valid.',
    'required'             => ' :attribute wajib diisi.',
    'required_if'          => ' :attribute wajib diisi ketika :or terdapat :value.',
    'required_unless'      => ' :attribute wajib diisi kecuali :or ada pada :values.',
    'required_with'        => ' :attribute wajib diisi ketika :values ada.',
    'required_with_all'    => ' :attribute wajib diisi ketika :values ada.',
    'required_without'     => ' :attribute wajib diisi ketika :values tidak ada.',
    'required_without_all' => ' :attribute diperluka jika tidak ada :values yg ada.',
    'same'                 => ' :attribute dan :or harus cocok.',
    'size'                 => [
        'numeric' => ' :attribute harus :size.',
        'file'    => ' :attribute harus :size kilobytes.',
        'string'  => ' :attribute harus :size characters.',
        'array'   => ' :attribute harus termasuk :size items.',
    ],
    'string'               => ' :attribute harus string.',
    'timezone'             => ' :attribute harus merupakan zona yang valid.',
    'unique'               => ' :attribute sudah diambil.',
    'uploaded'             => ' :attribute gagal mengunggah.',
    'url'                  => ' :attribute format tidak valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using 
    | convention "attribute.rule" to name  lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    |  following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
