<html>
<head>
    <title></title>
</head>
<body>
<style>
</style>
<table cellspacing="0" style="font-family: Roboto, Arial, sans-serif; border: 3px solid #000000; width: 800px; background: url('https://img-test.jba.co.id/jba-mobile/email/bg-logo.png') no-repeat 50% 50%;">
    <thead>
    <tr style="background-color: #2872b8; font-size: 1.5em;">
        <th colspan="2" style="color: #fff; padding: 20px 25px 20px 25px; text-align: left">JBA INDONESIA</th>
        <th colspan="3" style="color: #fff; padding: 20px 25px 20px 25px; text-align: right">SERAH TERIMA UNIT</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td colspan="3" style="border-bottom: 3px solid #000000;">
            <h1 style="margin-bottom: 5px; text-align: center; margin-top: 20px;">Terima Kasih!</h1>
            <h5 style="margin-top: 0; text-align: center;">
                Anda telah melakukan BASTK unit di JBA Indonesia
                <br><br>
                <b>{{ isset($spk_no) ? 'No. Ref. SPK : '. $spk_no .'-'. $no_bastk_idx : 'No. BASTK : '.$no_bastk_idx }}</b>
            </h5>
        </td>
        <td></td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Pengirim Nama </h5>
            <h4 style="margin-top: 0;"><b>{{ $pengirim_nama }}</b></h4>
        </td>
        <td width="20%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Penerima Nama</h5>
            <h4 style="margin-top: 0;"><b>{{ $penerima_nama }}</b></h4>
        </td>
        <td width="15%"></td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Pengirim No. HP </h5>
            <h4 style="margin-top: 0;"><b>{{ $pengirim_hp }}</b></h4>
        </td>
        <td width="20%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Penerima No. HP</h5>
            <h4 style="margin-top: 0;"><b>{{ $penerima_hp }}</b></h4>
        </td>
        <td width="15%"></td>
    </tr>
    <tr>
        <td width="15%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Pengirim Alamat </h5>
            <h4 style="margin-top: 0;"><b>{{ $pengirim_alamat }}</b></h4>
        </td>
        <td width="20%"></td>
        <td width="25%">
            <h5 style="margin-bottom: 5px; margin-top: 5px;">Penerima Alamat</h5>
            <h4 style="margin-top: 0;"><b>{{ $penerima_alamat }}</b></h4>
        </td>
        <td width="15%"></td>
    </tr>

    <tr>
        <td></td>
        <td colspan="3" style="border-bottom: 3px solid #000000;">&nbsp;</td>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td colspan="3"><h3>Detail Unit BASTK</h3></td>
        <td></td>
	</tr>
	
	<tr>
        <td></td>
        <td style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px;">No. Polisi</h5>
        </td>
        <td colspan="2" style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px; text-align: right;">{{ $no_polisi }}</h5>
        </td>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px;">Merk/Type</h5>
        </td>
        <td colspan="2" style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px; text-align: right;">{{ $make_name." ".$model_name }}</h5>
        </td>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px;">No. Rangka</h5>
        </td>
        <td colspan="2" style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px; text-align: right;">{{ $no_rangka }}</h5>
        </td>
        <td></td>
    </tr>
    
    <tr>
        <td></td>
        <td style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px;">No. Mesin</h5>
        </td>
        <td colspan="2" style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px; text-align: right;">{{ $no_mesin }}</h5>
        </td>
        <td></td>
    </tr>
    
    <tr>
        <td></td>
        <td style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px;">Pool</h5>
        </td>
        <td colspan="2" style="background-image: linear-gradient(to right, #a9a9a9 33%, rgba(255, 255, 255, 0) 0%); background-position: bottom; background-size: 8px 2px; background-repeat: repeat-x;">
        <h5 style="margin-bottom: 5px; text-align: right;">{{ $pool }}</h5>
        </td>
        <td></td>
    </tr>
	
    <tr>
        <td></td>
        <td colspan="3" style="border-bottom: 3px solid #000000;">&nbsp;</td>
        <td></td>
    </tr>

    <tr>
        <td></td>
        <td style="text-align: center;">
            <h5 style="margin-bottom: 10px; margin-top: 50px;">Yang Menyerahkan</h5>
			<img src="{{ env('S3_JBA_CAR_SIGNATURE_FOLDER') . $ttd_menyerahkan_img }}" style="width: auto; height: 80px;">
            <h4 style="margin-top: 10px;"><b>( {{ $menyerahkan_nama }} )</b></h4>
        </td>
        <td></td>
        <td style="text-align: center;">
            <h5 style="margin-bottom: 10px; margin-top: 50px;">Yang Menerima</h5>
			<img src="{{ env('S3_JBA_CAR_SIGNATURE_FOLDER') . $ttd_menerima_img }}" style="width: auto; height: 80px;">
            <h4 style="margin-top: 10px;"><b>( {{ $menerima_nama }} )</b></h4>
        </td>
        <td></td>
    </tr>	
	
    <tr>
        <td colspan="5" style="height: 10px;"></td>
    </tr>
    </tbody>
    <tfoot>
    <tr style="background-color: #2872b8;">
        <th colspan="5" style="height: 20px;"></th>
    </tr>
    </tfoot>
</table>
</body>
</html>