
## Judul Project
RESTful API "e-wallet"

## Penjelasan
### Framework
Framework yang digunakan adalah lumen versi 6 (karena versi ini adalah versi LTS, support bugs fixing dan security).
### Swagger
Untuk melayani proses testing telah disediakan API Documentation, agar memudahkan pengguna.
[{{base_url}}/api/documentation](http://localhost:8000/api/documentation)
1. x-api-key : token utama untuk bisa akses semua API
2. Authorization : token akses user hasil dari user login (Auth Bearer)

### Fitur
1. Login : untuk authentication user agar bisa mengakses aplikasi.
2. Logout : untuk keluar dari authentication / aplikasi.
3. Top up : untuk men-topup saldo user, dengan memasukan nominal yang ingin di topup dan kode bank.
4. Transfer : untuk transfer saldo ke akun user lain dan bisa transfer ke nomor sendiri. untuk transfer saldo ini perlu authentication password.
5. Check User : sebelum proses transfer, perlu checking data nomor pengguna yang akan di transfer apakah terdaftar di aplikasi dan saldo pengirim apakah mencukupi.

## Installing
langkah 1.
```
composer dump-autoload
```
langkah 2.
```
composer install
```
Langkah 3.
```
cp .env.example .env
```
Langakah 4.
```
php artisan migrate:refresh
```
Langakah 5.
```
php artisan db:seed
```
Langakah 6.
```
php artisan passport:client --personal
```
Langakah 7.
```
php -S localhost:8000 -t ./public
```

## Authors