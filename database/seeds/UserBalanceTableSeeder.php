<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserBalanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::select('id')->orderBy('id','ASC')->get();
        foreach ($users as $value) {
        	DB::table('user_balance')->insert([
	            'user_id' => $value->id,
	            'balance' => 0,
	            'balanceAchieve' => 100000000, // limit 10 juta
	            'created_at' => date("Y-m-d H:i:s"),
	            'updated_at' => date("Y-m-d H:i:s"),
	        ]);
        }
    }
}
