<?php

use Illuminate\Database\Seeder;
use App\Library\Bank;

class BankBalanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$bank = Bank::list();
        foreach ($bank as $key => $value) {
        	DB::table('bank_balance')->insert([
	            'balance' => 100000000, // saldo awal 100 juta
	            'balanceAchieve' => 100000000, // limit 100 juta
	            'enable' => 1,
	            'code' => $value,
	            'created_at' => date("Y-m-d H:i:s"),
	            'updated_at' => date("Y-m-d H:i:s"),
	        ]);
        }
    }
}
