<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankBalanceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_balance_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('bankBalanceId')->unsigned()->index();
            $table->foreign('bankBalanceId')->references('id')->on('bank_balance')->onDelete('cascade');
            $table->double('amount', 15,2);
            $table->double('balanceBefore', 15,2);
            $table->double('balanceAfter', 15,2);
            $table->string('activity');
            $table->enum('type', ['debit', 'kredit']);
            $table->string('ip');
            $table->string('location');
            $table->string('userAgent');
            $table->string('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_balance_history');
    }
}
