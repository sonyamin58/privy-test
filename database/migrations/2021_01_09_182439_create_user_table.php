<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id'); // autoincrement id field
            $table->string('name');   // string field
            $table->string('phone_number')->unique(); // unique string field
            $table->string('email')->unique(); // unique string field
            $table->string('password'); // string field with max 60 characters
            $table->boolean('active')->default(1); // string field with default value 1
            $table->boolean('premium')->default(0); // string field with default value 0
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
